{-# LANGUAGE StandaloneDeriving, TemplateHaskell, TypeApplications #-}

module Main where

--import Test.HUnit
import Control.Applicative
import Control.Monad (void)
import qualified Data.Map as M
import qualified Data.Set as S

import Test.QuickCheck
import Test.QuickCheck.All

import Logic
import Console

--Test that rotating 4 times is identity
polyRotate4 :: (Eq a, Rotate a) => a -> Bool
polyRotate4 a = a == rotateN 4 a

prop_rotate4Direction = polyRotate4 @Direction
prop_rotate4Street = polyRotate4 @Street

polyInverseRotate :: (Eq a, Rotate a) => a -> Int -> Bool
polyInverseRotate a n = rotateN (-n) (rotateN n a) == a

prop_inverseRotateDirection = polyInverseRotate @Direction
prop_inverseRotateStreet = polyInverseRotate @Street

instance Arbitrary Direction where
    arbitrary = Direction <$> choose (0, 3)

instance Arbitrary DetectivePos where
    arbitrary = DP <$> choose (0, 11)

instance Arbitrary Suspect where
    arbitrary = Suspect <$> arbitrary

--deriving Show Street

instance Arbitrary Street where
    arbitrary = liftA3 Street arbitrary arbitrary arbitrary

instance Arbitrary District where
    arbitrary = (District . M.fromList . zip (concat allCells)) <$> vectorOf 9 arbitrary

newtype BoundedCell = BC Cell

instance Show BoundedCell where
    show (BC c) = show c

instance Arbitrary BoundedCell where
    arbitrary = BC <$> liftA2 (,) (choose (0, 2)) (choose (0, 2))

testSuspects = map (Suspect . ("Bandit" ++) . show) [1 .. 9]
testStreets = [Street ss False [east, south, west] | ss <- testSuspects]
testDistrict = makeDistrict testStreets

newtype NineSuspects = NineSuspects [Suspect]
    deriving Show

instance Arbitrary NineSuspects where
    arbitrary = NineSuspects <$> vectorOf 9 arbitrary

--prop_visibleFirstLine :: [Suspect] -> Bool
prop_visibleFirstLine (NineSuspects suspects) =
    firstLine == suspectsOf (visibleStreets district (DP 3))  &&
    firstLine == suspectsOf (visibleStreets district (DP 11)) &&
    S.empty   == suspectsOf (visibleStreets district (DP 0)) where
    firstLine = suspectsOf $ take 3 streets
    streets = [Street ss False [west, east] | ss <- suspects]
    district = makeDistrict streets
    suspectsOf = S.fromList . map (ssName . sSuspect)
    {- Test district

    -1- -2- -3-
     v   v   v
    -4- -5- -6-
     v   v   v
    -7- -8- -9-
     v   v   v
    -}

instance Eq District where
    (District d1) == (District d2) = d1 == d2

-- Action tests
prop_inverseRotateStreetIn :: BoundedCell -> Int -> District -> Bool
prop_inverseRotateStreetIn (BC c) off d = rotateStreet c (-off) (rotateStreet c off d) == d

prop_commutativeSwap :: BoundedCell -> BoundedCell -> District -> Bool
prop_commutativeSwap (BC c1) (BC c2) d = swapStreets c1 c2 d == swapStreets c2 c1 d

prop_doubleSwap :: BoundedCell -> BoundedCell -> District -> Bool
prop_doubleSwap (BC c1) (BC c2) d = swapStreets c1 c2 (swapStreets c1 c2 d) == d

-- Win conditions tests
prop_suspectFound :: BoundedCell -> District -> Bool
prop_suspectFound (BC c) d = checkSuspectFound ls == Just MrDetective where
    ls = makeState flipAll deck [] []
    flipAll = map (\s -> s {sFlipped = True}) $ allStreets d
    deck = map (\s -> Card (sSuspect s) 0) $ allStreets d
    onlySusp = Card $ sSuspect $ d `getStreet` c

--prop_differentTokens :: [

return []
runTests = $quickCheckAll

main :: IO ()
main = void runTests
