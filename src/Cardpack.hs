module Cardpack (initState) where

import Logic

standardExits = [north, east, south]
suspectData =
    [ (Suspect "Insp. Lestrade", 0)
    , (Suspect "Sgt. Goodley", 0)
    , (Suspect "John Pizer", 1)
    , (Suspect "Madame", 2)
    , (Suspect "Miss Stealthy", 1)
    , (Suspect "John Smith", 1)
    , (Suspect "Jeremy Bert", 1)
    , (Suspect "Joseph Lane", 1)
    , (Suspect "William Gull", 1)
    ]

streets = map (makeStreet . fst) suspectData where
    makeStreet name = Street name False standardExits

deck = map (uncurry Card) suspectData

detectives@[holmes, watson, toby] = map Detective ["Holmes", "Watson", "Toby"]

tokens =
    [ Token (TokMove holmes) TokRotate
    , Token (TokMove watson) TokSwap
    , Token (TokMove toby)   TokMoveAny
    , Token TokDrawCard      TokRotate
    ]

initState :: LogicState
initState = makeState streets deck detectives tokens
