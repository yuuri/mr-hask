module Logic where

import Control.Monad
import Data.Function (on)
import Data.List (delete, nub)
import Data.Monoid
import qualified Data.Map as M
import qualified Data.Set as S
import System.Random

-- Basic types --

-- |Class of things which can be rotated clockwize.
-- |Minimal complete definition: 'rotate' or 'rotateN'
class Rotate a where
    rotate :: a -> a
    rotate = rotateN 1
    rotateN :: Int -> a -> a
    rotateN 0 a = a
    rotateN n a = rotateN (n - 1) a

type Cell = {- #UNPACK -} (Int, Int)
allCells = [[(0, 0), (1, 0), (2, 0)], [(0, 1), (1, 1), (2, 1)], [(0, 2), (1, 2), (2, 2)]] :: [[Cell]]
--data Cell = Cell !Int !Int
--allCells = map (map (zip Cell)) [[(0, 0), (1, 0), (2, 0)], [(0, 1), (1, 1), (2, 1)], [(0, 2), (1, 2), (2, 2)]]

-- |Direction, starting north, numbering clockwize
-- |    ^ 0
-- |3 < + > 1
-- |    v 2
newtype Direction = Direction Int
    deriving Eq

instance Show Direction where
    show (Direction n) = show n

instance Rotate Direction where
    rotateN n (Direction dir) = Direction $ (dir + n) `mod` 4

-- |Mnemonics, just in case
allDirections@[north, east, south, west] = map Direction [0 .. 3]

dirToOffset :: Direction -> Cell
dirToOffset (Direction dir) = [(0,-1),(1,0),(0,1),(-1,0)] !! dir

-- Game items --

-- |Two players
data Player = MrDetective | MrJack
    deriving (Eq, Show)

nextPlayer :: Player -> Player
nextPlayer MrDetective = MrJack
nextPlayer MrJack = MrDetective

newtype Suspect = Suspect
    { ssName :: String
    }
    deriving (Eq, Ord)

instance Show Suspect where
    show = ssName

-- |Detectives; uniquely identified by name
newtype Detective = Detective
    { dtName :: String
    }
    deriving (Eq, Ord)

instance Show Detective where
    show = dtName

-- |Tile with a street
data Street = Street
    { sSuspect :: !Suspect
    , sFlipped :: !Bool
    , sExits   :: [Direction]
    }

instance Eq Street where
    (==) = (==) `on` sSuspect

instance Ord Street where
    compare = compare `on` sSuspect

instance Rotate Street where
    rotateN n s@Street {sExits = exits} =
        s {sExits = map (rotateN n) exits}

-- |District of nine streets
newtype District = District (M.Map Cell Street)

-- |Makes district from nine streets, enumerated left-to-right, up-to-down
makeDistrict :: [Street] -> District
makeDistrict = District . M.fromList . zip (concat allCells)

lookupStreet :: Cell -> District -> Maybe Street
lookupStreet c (District d) = M.lookup c d

getStreet :: District -> Cell -> Street
getStreet (District d) = (d M.!)

allStreets :: District -> [Street]
allStreets (District d) = M.elems d

rotateStreet :: Cell -> Int -> District -> District
rotateStreet c n (District d) = District $ M.adjust (rotateN n) c d

swapStreets :: Cell -> Cell -> District -> District
swapStreets c1 c2 (District d) = District $ M.insert c1 s2 $ M.insert c2 s1 d where
    s1 = d M.! c1; s2 = d M.! c2

-- |Deck of suspect cards
data Card = Card
    { crSuspect   :: !Suspect
    , crHourglass :: !Int
    }

type Deck = [Card]

-- |Detective position onboard; starting from top-left corner, clockwise:
-- |   0 1 2
-- |11 [][][] 3
-- |10 [][][] 4
-- | 9 [][][] 5
-- |   8 7 6
newtype DetectivePos = DP Int
    deriving Eq

instance Show DetectivePos where
    show (DP n) = show n

moveDetective :: Int -> DetectivePos -> DetectivePos
moveDetective off (DP d) = DP $ (d + off) `rem` 12

--instance Rotate DetectivePos where
--    rotateN n (DP dp) = DP $ (dp + n * 3) `rem` 12

dpToDir :: DetectivePos -> Direction
dpToDir (DP pos) = Direction $ pos `quot` 3

dpToCell :: DetectivePos -> Cell
dpToCell (DP pos) = [(0,0),(1,0),(2,0),(2,0),(2,1),(2,2),(2,2),(1,2),(0,2),(0,2),(0,1),(0,0)] !! pos

-- |Returns the list of streets which are visible from the given position.
visibleStreets :: District -> DetectivePos -> [Street]
visibleStreets district dp = trace (dpToCell dp) where
    (offX, offY) = dirToOffset exit
    enter = dpToDir dp
    exit = rotateN 2 enter
    trace pos@(x, y) = case lookupStreet pos district of
        Nothing -> [] --out of the map
        Just str -> case (enter `elem` sExits str, exit `elem` sExits str) of
            (False, _)    -> []   --this cell is unlookable
            (True, False) -> [str] --can't look further
            (True, True)  -> str : trace (x + offX, y + offY)

flipStreets :: [Street] -> District -> District
flipStreets ss (District d) = District $ M.map (\e -> if e `elem` ss then e {sFlipped = True} else e) d

-- |Actions on tokens
data ActionToken
    = TokMove !Detective
    | TokMoveAny
    | TokRotate
    | TokSwap
    | TokDrawCard
    deriving Eq

-- |Two-sided action tokens
data Token = Token
    { tokFront, tokBack :: !ActionToken
    }

flipToken (Token front back) = Token back front
flipTokens = map flipToken

throwTokens :: [Token] -> IO [Token]
throwTokens tokens = zipWith maybeFlip tokens `liftM` replicateM 4 randomIO where
    maybeFlip token True  = flipToken token
    maybeFlip token False = token


-- |Info about the current turn
data Turn = Turn
    { turnNumber :: !Int
    , turnPlayer :: !Player
    , turnActions :: [ActionToken]
    }

nextTurn :: Turn -> Turn
nextTurn (Turn n p [_]) = Turn (n + 1) (nextPlayer p) []
nextTurn t@(Turn _ _ ac@[_,_,_]) = t
nextTurn (Turn n p ac) = Turn n (nextPlayer p) ac

-- |Game state
data LogicState = LS
    { lsDistrict :: !District
    , lsDetectives :: !(M.Map Detective DetectivePos)
    , lsDeck    :: Deck
    , lsTokens  :: [Token]
    , lsTurn    :: !Turn
    , lsMDHand, lsMJHand :: [Card]
    }

initDetectives ds = M.fromList $ zip ds $ map DP [11, 3, 7]

detectivePositions :: LogicState -> [(Detective, DetectivePos)]
detectivePositions = M.toList . lsDetectives

makeState :: [Street] -> Deck -> [Detective] -> [Token] -> LogicState
makeState ss (jack:rest) ds ts = LS (makeDistrict ss) (initDetectives ds) rest ts turn1 [] [jack] where
    turn1 = Turn 1 MrDetective (map tokFront ts)

currentPlayer :: LogicState -> Player
currentPlayer = turnPlayer . lsTurn

allVisibleStreets :: LogicState -> [Street]
allVisibleStreets ls = res where
    allPos = nub $ M.elems $ lsDetectives ls
    res = nub $ concatMap (visibleStreets $ lsDistrict ls) allPos

-- |Concrete action
data Action
    = Move !Detective !Int
    | Rotate !Cell !Int
    | Swap !Cell !Cell
    | DrawCard
    deriving Eq

-- TODO: check if action is valid
data Error
    = UnavailAction
    | OutOfBorder
    | InvalidMove

checkAction :: Action -> [Token]
checkAction = undefined

applyAction :: Action -> LogicState -> LogicState
applyAction DrawCard ls@LS {lsTurn = turn, lsDeck = c:cs, lsMDHand = mdHand, lsMJHand = mjHand} =
    ls {lsDeck = cs, lsMDHand = mdHand', lsMJHand = mjHand'} where
        --TODO: refactor
        (mdHand', mjHand') = if currentPlayer ls == MrJack then (mdHand, c:mjHand) else (c:mdHand, mjHand)
applyAction (Move d off) ls@LS {lsDetectives = ds} =
    ls {lsDetectives = M.adjust (moveDetective off) d ds}
applyAction (Rotate c n) ls@LS {lsDistrict = d} = ls {lsDistrict = rotateStreet c n d}
applyAction (Swap c1 c2) ls@LS {lsDistrict = d} = ls {lsDistrict = swapStreets c1 c2 d}

tryDelete :: Eq a => a -> [a] -> (Bool, [a])
tryDelete x xs = if x `elem` xs then (True, delete x xs) else (False, xs)

data TurnResult
    = WrongTurn Error
    | NextPhase LogicState
    | NextTurn (Bool, LogicState)
    | GameOver Player

tryAction :: ActionToken -> Action -> LogicState -> TurnResult
tryAction TokMoveAny (Move _ off) _ | off /= 1 = WrongTurn InvalidMove
tryAction (TokMove d1) (Move d2 off) _ | d1 /= d2 || off < 1 || off > 2 = WrongTurn InvalidMove
tryAction TokSwap (Swap (x1, y1) (x2, y2)) _ | any (\c -> c < 0 || c > 2) [x1, x2, y1, y2] = WrongTurn OutOfBorder
tryAction TokRotate (Rotate (x, y) _) _ | any (\c -> c < 0 || c > 2) [x, y] = WrongTurn OutOfBorder
tryAction token action ls@LS {lsTurn = turn, lsTokens = tokens} = case tryDelete token (turnActions turn) of
    (False, _) -> WrongTurn UnavailAction
    (True, []) -> NextTurn (jackVisible, newState {lsDistrict = newDistrict, lsTurn = newTurn {turnActions = newActions}, lsTokens = newTokens}) where --TODO: flip for MrDetective
        newTokens = flipTokens tokens
        newActions = map tokFront newTokens
        ssVis = S.fromList $ allVisibleStreets newState
        ssAll = S.fromList $ allStreets $ lsDistrict newState
        jackVisible = crSuspect (last $ lsMJHand newState) `S.member` S.map sSuspect ssVis
        newDistrict = flipStreets (S.toList $ if jackVisible then ssAll S.\\ ssVis else ssVis) (lsDistrict newState)
    (True, newAc) -> NextPhase $ newState {lsTurn = newTurn {turnActions = newAc}}
    where
        newState = applyAction action (ls {lsTurn = newTurn})
        newTurn  = nextTurn turn

type WinChecker = LogicState -> Maybe Player

checkSuspectFound :: WinChecker
checkSuspectFound ls =
    if length (filter sFlipped $ allStreets $ lsDistrict ls) >= 8
    then Just MrDetective
    else Nothing

checkOvertime :: WinChecker
checkOvertime ls =
    if turnNumber (lsTurn ls) > 8
    then Just MrJack
    else Nothing

checkConfused :: WinChecker
checkConfused ls =
    if sum (map crHourglass $ lsMJHand ls) >= 6
    then Just MrJack
    else Nothing

-- |Checks if the game is over and who is the winner then
isGameOver :: LogicState -> Maybe Player
isGameOver ls = getFirst . mconcat . map (First . ($ ls)) $
    [checkSuspectFound, checkOvertime, checkConfused]
