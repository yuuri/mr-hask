module Console where

import Control.Monad (liftM)
import Data.List (transpose)
import Text.Printf

import Logic
import Cardpack

--instance Show ActionToken where
--    show (ActionToken front back) = show (front, back)

-- Street cells are odd-sized (to have exits at the middle of edges),
-- so full size is 2*streetHalfWidth+1 X 2*streetHalfHeight*2+1
streetHalfWidth = 4 :: Int
streetHalfHeight = 2 :: Int

showStreet :: Street -> [String]
showStreet (Street suspect flipped exits) = [firstLine, secondLine, thirdLine] where
    [no, eo, so, wo] = map (`elem` exits) allDirections
    labelWidth = 2 * streetHalfWidth - 1
    firstLine = replicate streetHalfWidth '-' ++ (if no then '#' else '-'):replicate streetHalfWidth '-'
    secondLine = (if wo then '#' else '|'):take labelWidth ((if flipped then "" else ssName suspect) ++ repeat ' ') ++ (if eo then "#" else "|")
    thirdLine = replicate streetHalfWidth '-' ++ (if so then '#' else '-'):replicate streetHalfWidth '-'

instance Show Street where
    show = unlines . showStreet

showDistrict :: District -> [String]
showDistrict district = concatMap (map concat . transpose) ls where
    --combine :: [String] -> String
    --combine = map concat . transpose
    ls :: [[[String]]]
    ls = [map (showStreet . getStreet district) line | line <- allCells]

instance Show District where
    show = unlines . showDistrict

-- TODO
instance Show ActionToken where
    show TokDrawCard = "[draw] a suspect card"
    show (TokMove d) = printf "[Move] %s by 1 or 2 steps clockwise" (show d)
    show TokSwap     = "[swap] any two streets in the district"
    show TokRotate   = "[rotate] any street by 90, 180 or 270 degrees"
    show TokMoveAny  = "[move1] any detective by 1 step clockwise"

instance Show Token where
    show (Token f _) = printf "[%s/?]" (show f)

instance Show LogicState where
    show ls@(LS district detectives deck tokens turn mdHand mjHand) = unlines $ header : board ++ footer where
        Turn {turnNumber = n, turnActions = ac} = turn
        header = printf "Turn: %d (%s)\n" n (show $ currentPlayer ls)
        board = northDs : zipWith3 (\a b c -> a : b ++ [c]) westDs (showDistrict district) eastDs
        --Todo: show detectives
        northDs = ""
        southDs = ""
        westDs = repeat ' '
        eastDs = westDs
        --Mockup
        detectives = unwords $ map (\(Detective d, DP p) -> d ++ ": " ++ show p) $ detectivePositions ls
        footer = detectives : "You may: " : map show ac

instance Show Error where
    show UnavailAction = "You can't perform this action"
    show OutOfBorder   = "Please choose streets inside the district"
    show InvalidMove   = "Move distance is invalid"

-- TODO: use parsers
{-instance Read Action where
    readsPrec _ s = [ac, r] where
        (action, r1) = lex r1
        (ac, r) = case action of
            "move"   -> liftM2 Move reads reads
            "swap"   -> liftM2 Swap reads reads
            "rotate" -> liftM2 Rotate reads reads
            "draw"   -> return . Draw
        --case words xs of
        --["move", d, off] -> Move (read d)-}

parseAction str = case words str of
    ["move", d, off] -> let d' = Detective d in Just (TokMove d', Move d' (read off))
    ["move1", d] -> Just (TokMoveAny, Move (Detective d) 1)
    ["swap", c1, c2] -> Just (TokSwap, Swap (read c1) (read c2))
    ["rotate", c, off] -> Just (TokRotate, Rotate (read c) (read off `quot` 90))
    ["draw"] -> Just (TokDrawCard, DrawCard)
    _ -> Nothing

playGame :: IO ()
playGame = go initState where
    go ls = do
        print ls
        putStrLn $ show (currentPlayer ls) ++ ", what to do now?"
        prompt ls
    prompt ls = do
        parseRes <- parseAction `liftM` getLine
        case parseRes of
            Nothing -> putStrLn "Wrong command" >> prompt ls
            Just (token, action) -> case tryAction token action ls of
                WrongTurn err -> print err >> prompt ls
                NextPhase newLS -> go newLS
                NextTurn (jackVisible, newLS) -> do
                    putStrLn $ "End of turn, Jack is " ++ if jackVisible then "visible" else "invisible"
                    go newLS
                GameOver p -> putStrLn $ printf "The winner is %s!" (show p)
